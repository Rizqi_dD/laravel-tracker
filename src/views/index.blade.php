@extends($stats_layout)

@section('title', trans($title))

@section('pageStyles')
<link rel="stylesheet"type="text/css"href="{{ $stats_template_path }}/lafeber/world-flags-sprite/flags16.css"/>
@endsection

@section('content')
<!-- Main content -->
<div class="content-wrapper">
    <div class="row">
        <div class="col-sm-12">
            <!-- Media library -->
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Tracker Visits</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="reload" onclick="RefreshTableAjax();"></a></li>
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>
                <table id="table_div" class="table" cellspacing="0" width="100%"></table>
            </div>
            <!-- /media library -->
        </div>
    </div>
</div>
<!-- /main content -->

@endsection

@section('pageScripts')
	<script src="{{ URL::asset('backend/js/plugins/tables/datatables/datatables.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::asset('backend/js/plugins/tables/datatables/extensions/buttons.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('backend/js/plugins/tables/datatables/extensions/responsive.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('backend/js/plugins/forms/selects/select2.min.js') }}" type="text/javascript"></script>
@endsection

@push('subPageScripts')
   <script> @include('cyberexploits/tracker::_datatables', $datatables_data) </script>
@endpush