@extends($stats_layout)

@section('title', trans($title))

@section('pageStyles')
@endsection

@section('content')
<!-- Main content -->
<div class="content-wrapper">
    <div class="row">
        <div class="col-sm-12">
            <!-- Media library -->
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Tracking Errors</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="reload" onclick="RefreshTableAjax();"></a></li>
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>
                <table id="table_div" class="table" cellspacing="0" width="100%"></table>
            </div>
            <!-- /media library -->
        </div>
    </div>
</div>
<!-- /main content -->

@endsection

@section('pageScripts')
	<script src="{{ URL::asset('backend/js/plugins/tables/datatables/datatables.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::asset('backend/js/plugins/tables/datatables/extensions/buttons.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('backend/js/plugins/tables/datatables/extensions/responsive.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('backend/js/plugins/forms/selects/select2.min.js') }}" type="text/javascript"></script>
@endsection

@push('subPageScripts')
   <script> 
   @include(
        'cyberexploits/tracker::_datatables',
        array(
            'datatables_ajax_route' => route('tracker.stats.api.errors'),
            'datatables_columns' =>
            '
                { "data" : "error.code",     "title" : "'.trans('code').'" },
                { "data" : "session.uuid",   "title" : "'.trans('session').'" },
                { "data" : "error.message",  "title" : "'.trans('message').'" },
                { "data" : "path.path",      "title" : "'.trans('path').'" },
                { "data" : "updated_at",     "title" : "'.trans('when').'" },
            '
        )
    )
   </script>
@endpush
