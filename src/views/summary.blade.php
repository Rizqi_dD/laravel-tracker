@extends($stats_layout)

@section('title', title_case(trans($title)))

@section('pageStyles')
<link rel="stylesheet"type="text/css"href="{{ $stats_template_path }}/lafeber/world-flags-sprite/flags16.css"/>
<link href="{{ $stats_template_path }}/morrisjs/morris.css" rel="stylesheet">@endsection
@section('content')
<!-- Main content -->
<div class="content-wrapper">
    <div class="row">
        <div class="col-sm-6">
            <!-- Media library -->
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Page View Summary</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="reload" onclick="RefreshTableAjax();"></a></li>
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>
                <div id="pageViewsLine" class="chart no-padding" style="height: 450px;"></div>
            </div>
            <!-- /media library -->
        </div>
        <div class="col-sm-6">
            <!-- Media library -->
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">@lang('Page Views by Country')</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="reload" onclick="RefreshTableAjax();"></a></li>
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>
				<div id="pageViewsByCountry" style="height: 450px;"></div>
            </div>
            <!-- /media library -->
        </div>
    </div>
</div>
<!-- /main content -->

@endsection
@section('pageScripts')	
	<!-- Page-Level Plugin Scripts - Main -->
	<script src="{{ $stats_template_path }}/raphael/raphael.min.js"></script>
	<script src="{{ $stats_template_path }}/morrisjs/morris.min.js"></script>

	<!-- Page-Level Plugin Scripts - Flot -->
	<!--[if lte IE 8]><script src="{{ $stats_template_path }}/js/excanvas.min.js"></script><![endif]-->
	<script src="{{ $stats_template_path }}/flot/jquery.flot.js"></script>
	<script src="{{ $stats_template_path }}/flot/jquery.flot.resize.js"></script>
	<script src="{{ $stats_template_path }}/flot/jquery.flot.pie.js"></script>
    <script src="{{ $stats_template_path }}/flot-tooltip/jquery.flot.tooltip.min.js"></script>
@endsection

@push('subPageScripts')
   <script> 
   jQuery(function()
    {
		//console.log(jQuery('#pageViews'));

		var pageViewsLine = Morris.Line({
            element: 'pageViewsLine',
            parseTime:false,
			grid: true,
			data: [{'date': 0, 'total': 0}],
			xkey: 'date',
			ykeys: ['total'],
			labels: ['Page Views']
		});

		jQuery.ajax({
			type: "GET",
			url: "{{ route('tracker.stats.api.pageviews') }}",
			data: { }
		})
		.done(function( data ) {
		   // console.log(data);
			pageViewsLine.setData(formatDates(data));
		});

		var convertToPlottableData = function(data)
		{
			plottable = [];

			jsondata = JSON.parse(data);

            for(key in jsondata)
            {
                plottable[key] = {
					label: jsondata[key].label,
					data: jsondata[key].value
				}
            }

			return plottable;
        };

		var formatDates = function(data)
        {
			data = JSON.parse(data);

            for(key in data)
            {
                if (data[key].date !== 'undefined')
                {
					data[key].date = moment(data[key].date, "YYYY-MM-DD").format('dddd[,] MMM Do');
				}
            }

			return data;
		};
	});
    </script>
    @include('cyberexploits/tracker::_summaryPiechart')
@endpush