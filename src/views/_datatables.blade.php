$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('#table_div').DataTable( {
        "autoWidth": false,
        "processing": true,
        "serverSide": true,
        "responsive": true,
        "ajax": "{{$datatables_ajax_route}}",
        "columnDefs": [ {
            "targets": "_all",
            "defaultContent": ""
        } ],
        "columns": [
            <?php echo $datatables_columns; ?>
        ],
        pageLength: 10,
        lengthMenu: [
            [5, 10, 25,50,100],
            [5, 10, 25,50,100] // change per page values here
        ],
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        buttons: [{
                    extend: 'colvis',
                    text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                    className: 'btn btn-xs bg-blue btn-icon',
                    collectionLayout: 'fixed two-column'
                }
            ]
    } );

    // Launch Uniform styling for checkboxes
    $('.ColVis_Button').addClass('btn btn-primary btn-icon').on('click mouseover', function() {
        $('.ColVis_collection input').uniform();
    });

    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });
} );
