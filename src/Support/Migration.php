<?php

namespace CyberExploits\Tracker\Support;

use CyberExploits\Support\Migration as cyberexploitsMigration;

abstract class Migration extends cyberexploitsMigration
{
    protected function checkConnection()
    {
        $this->manager = app()->make('db');

        $this->connection = $this->manager->connection('tracker');

        parent::checkConnection();
    }
}
