<?php

namespace CyberExploits\Tracker\Support\Exceptions;

use ErrorException;

class Error extends ErrorException
{
}
