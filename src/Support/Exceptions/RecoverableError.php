<?php

namespace CyberExploits\Tracker\Support\Exceptions;

use Exception;

class RecoverableError extends Exception
{
}
