<?php

namespace CyberExploits\Tracker\Vendor\Laravel\Controllers;

use Datatables;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\View;
use CyberExploits\Tracker\Vendor\Laravel\Facade as Tracker;
use CyberExploits\Tracker\Vendor\Laravel\Support\Session;

class Stats extends Controller
{
    public function __construct()
    {
        $this->authentication = app()->make('tracker.authentication');
    }

    public function index(Session $session)
    {
        if (!$this->isAuthenticated()) {
            return redirect('admin/login');
        }
       return $this->showPage($session, $session->getValue('page'));
    }

    /**
     * @param Session $session
     */
    public function showPage($session, $page)
    {
        $me = $this;
        
        if (method_exists($me, $page)) {
            return $this->$page($session);
        }
    }

    public function visits(Session $session)
    {
        $datatables_data =
        [
            'datatables_ajax_route' => route('tracker.stats.api.visits'),
            'datatables_columns'    => '
                { "data" : "id",          "title" : "'.trans('id').'"},
                { "data" : "client_ip",   "title" : "'.trans('ip address').'"},
                { "data" : "country",     "title" : "'.trans('country city').'"},
                { "data" : "user",        "title" : "'.trans('user').'"},
                { "data" : "device",      "title" : "'.trans('device').'"},
                { "data" : "browser",     "title" : "'.trans('browser').'"},
                { "data" : "language",    "title" : "'.trans('language').'"},
                { "data" : "referer",     "title" : "'.trans('referer').'"},
                { "data" : "pageViews",   "title" : "'.trans('page views').'"},
                { "data" : "lastActivity","title" : "'.trans('last activity').'"},
            ',
        ];

        return View::make('cyberexploits/tracker::index')
            ->with('sessions', Tracker::sessions($session->getMinutes()))
            ->with('title', ''.trans('visits').'')
            ->with('username_column', Tracker::getConfig('authenticated_user_username_column'))
            ->with('datatables_data', $datatables_data);
    }

    public function log($uuid)
    {
        $session = Tracker::sessionLog($uuid);

        return View::make('cyberexploits/tracker::log')
                ->with('log', Tracker::sessionLog($uuid))
                ->with('uuid', $uuid)
                ->with('title', 'log');
    }

    public function summary()
    {
        return View::make('cyberexploits/tracker::summary')
                ->with('title', ''.trans('page views summary').'');
    }

    public function apiPageviews(Session $session)
    {
        return Tracker::pageViews($session->getMinutes())->toJson();
    }

    public function apiPageviewsByCountry(Session $session)
    {
        return Tracker::pageViewsByCountry($session->getMinutes())->toJson();
    }

    public function apiLog($uuid)
    {
        $query = Tracker::sessionLog($uuid, false);
        $query->select([
            'id',
            'session_id',
            'method',
            'path_id',
            'query_id',
            'route_path_id',
            'is_ajax',
            'is_secure',
            'is_json',
            'wants_json',
            'error_id',
            'created_at',
        ]);
        return Datatables::of($query)
            ->addColumn('route_name', function ($row) {
                $path = $row->routePath;

            return  $row->routePath
                    ? $row->routePath->route->name.'<br>'.$row->routePath->route->action
                    : ($row->path ? $row->path->path : '');
            })

            ->addColumn('route', function ($row) {
                $route = null;

                if ($row->routePath) {
                    foreach ($row->routePath->parameters as $parameter) {
                        $route .= ($route ? '<br>' : '').$parameter->parameter.'='.$parameter->value;
                    }
                }

                return $route;
            })

            ->addColumn('query', function ($row) {
                $query = null;

                if ($row->logQuery) {
                    foreach ($row->logQuery->arguments as $argument) {
                        $query .= ($query ? '<br>' : '').$argument->argument.'='.$argument->value;
                    }
                }

                return $query;
            })

            ->editColumn('is_ajax', function ($row) {
                return    $row->is_ajax ? 'yes' : 'no';
            })

            ->editColumn('is_secure', function ($row) {
                return    $row->is_secure ? 'yes' : 'no';
            })

            ->editColumn('is_json', function ($row) {
                return    $row->is_json ? 'yes' : 'no';
            })

            ->editColumn('wants_json', function ($row) {
                return    $row->wants_json ? 'yes' : 'no';
            })

            ->addColumn('error', function ($row) {
                return    $row->error ? 'yes' : 'no';
            })

            ->make(true);
    }

    public function users(Session $session)
    {
        return View::make('cyberexploits/tracker::users')
            ->with('users', Tracker::users($session->getMinutes()))
            ->with('title', ''.trans('users').'')
            ->with('username_column', Tracker::getConfig('authenticated_user_username_column'));
    }

    public function events(Session $session)
    {
        return View::make('cyberexploits/tracker::events')
            ->with('events', Tracker::events($session->getMinutes()))
            ->with('title', ''.trans('events').'');
    }

    public function errors(Session $session)
    {
        return View::make('cyberexploits/tracker::errors')
            ->with('error_log', Tracker::errors($session->getMinutes()))
            ->with('title', ''.trans('errors').'');
    }

    public function apiErrors(Session $session)
    {
        $query = Tracker::errors($session->getMinutes(), false);
        $query->select([
                        'tracker_log.id',
                        'tracker_log.error_id',
                        'tracker_log.session_id',
                        'tracker_log.path_id',
                        'tracker_log.created_at',
                        'tracker_log.updated_at',
                        ]);
      
        return Datatables::of($query)
                ->editColumn('tracker_log.updated_at', function ($row) {
                    return "{$row->updated_at->diffForHumans()}";
                })
                ->make(true);
    }

    public function apiEvents(Session $session)
    {
        $query = Tracker::events($session->getMinutes(), false);

        return Datatables::of($query)->make(true);
    }

    public function apiUsers(Session $session)
    {
        $username_column = Tracker::getConfig('authenticated_user_username_column');

        return Datatables::of(Tracker::users($session->getMinutes(), false))
                ->editColumn('user_id', function ($row) use ($username_column) {
                    return "{$row->user_id}";
                    // return "{$row->user->$username_column}";
                })
                ->editColumn('updated_at', function ($row) {
                    return "{$row->updated_at->diffForHumans()}";
                })
                ->make(true);
    }

    public function apiVisits(Session $session)
    {
        $username_column = Tracker::getConfig('authenticated_user_username_column');

        $query = Tracker::sessions($session->getMinutes(), false);

        $query->select([
                'id',
                'uuid',
                'user_id',
                'device_id',
                'agent_id',
                'client_ip',
                'referer_id',
                'cookie_id',
                'geoip_id',
                'language_id',
                'is_robot',
                'updated_at',
        ]);
        return Datatables::of($query)
                ->editColumn('id', function ($row) {
                    $uri = route('tracker.stats.visits.log', $row->uuid);

                    return '<a href="'.$uri.'">'.$row->id.'</a>';
                })

                ->addColumn('country', function ($row) {
                    $cityName = $row->geoip && $row->geoip->city ? ' - '.$row->geoip->city : '';

                    $countryName = ($row->geoip ? $row->geoip->country_name : '').$cityName;

                    $countryCode = strtolower($row->geoip ? $row->geoip->country_code : '');

                    $flag = $countryCode
                            ? "<span class=\"f16\"><span class=\"flag $countryCode\" alt=\"$countryName\" /></span></span>"
                            : '';

                    return "$flag $countryName";
                })

                ->addColumn('user', function ($row) use ($username_column) {
                    return $row->user_id ? $row->user_id : 'guest';
                    // return $row->user ? $row->user->$username_column : 'guest';
                })

                ->addColumn('device', function ($row) {
                    $model = ($row->device && $row->device->model && $row->device->model !== 'unavailable' ? '['.$row->device->model.']' : '');

                    $platform = ($row->device && $row->device->platform ? ' ['.trim($row->device->platform.' '.$row->device->platform_version).']' : '');

                    $mobile = ($row->device && $row->device->is_mobile ? ' [mobile device]' : '');

                    return $model || $platform || $mobile
                            ? $row->device->kind.' '.$model.' '.$platform.' '.$mobile
                            : '';
                })

                ->addColumn('browser', function ($row) {
                    return $row->agent && $row->agent
                            ? $row->agent->browser.' ('.$row->agent->browser_version.')'
                            : '';
                })

                ->addColumn('language', function ($row)  {
                    return $row->language && $row->language
                        ? $row->language->preference
                        : '';
                })

                ->addColumn('referer', function ($row) {
                    return $row->referer ? $row->referer->domain->name : '';
                })

                ->addColumn('pageViews', function ($row)  {
                    return $row->page_views;
                })

                ->addColumn('lastActivity', function ($row) {
                    return $row->updated_at->diffForHumans();
                })

                ->rawColumns(['id','country'])->make(true);
    }

    private function isAuthenticated()
    {
        return $this->authentication->check();
    }
}
