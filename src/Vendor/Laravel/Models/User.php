<?php

namespace CyberExploits\Tracker\Vendor\Laravel\Models;

class User extends Base
{
    protected $primaryKey = 'username';

    public function getIsAdminAttribute()
    {
        return true;
    }
}
